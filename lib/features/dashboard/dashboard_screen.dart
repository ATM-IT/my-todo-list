import 'package:flutter/material.dart';
import 'package:mytodolist/widget/base_screen.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  _getBody(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
            color: Color(0xFF0A0812),
            //color: Color(0xFFEAEFF7).withOpacity(1),
            // color: Color(0xFF2659CA).withOpacity(1),
            //color: Color(0xFF4A90E2).withOpacity(0.5),
            /*gradient: LinearGradient(
              colors: [
                Color(0xFF5178FF),
                Color(0xFF5178FF),
                Color(0xFF54FFC4),
                Color(0xFF4A90E2),
                Color(0xFF5178FF),
              ],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              // Add one stop for each color. Stops should increase from 0 to 1
              stops: [0.1, 0.3, 0.5, 0.7, 0.9],
            ),*/
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(_getBody(context));
  }
}
