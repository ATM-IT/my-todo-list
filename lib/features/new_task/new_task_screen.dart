import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytodolist/core/service/notification_service.dart';
import 'package:mytodolist/features/new_task/task_button_submit.dart';
import 'package:mytodolist/features/new_task/task_calendar.dart';
import 'package:mytodolist/state_management/calendar/calendar_bloc.dart';

class NewTaskScreen extends StatelessWidget {
  const NewTaskScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future<TimeOfDay?> show() async {
      return showTimePicker(context: context, initialTime: TimeOfDay.now());
    }

    NotificationService _notificationService = NotificationService();
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFF0A0812),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          // brightness: Brightness.light,
          elevation: 0.0,
          title: const Text(
            'Now Playing',
            style: TextStyle(
              color: Color(0xFF4A4A4A),
              fontSize: 17.0,
            ),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.keyboard_arrow_down,
              size: 20.0,
              color: Color(0xFFFE4046),
            ),
            onPressed: () {},
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.menu,
                color: Color(0xFFFE4046),
              ),
              onPressed: () {},
            ),
          ],
        ),
        body: BlocProvider(
          create: (context) => CalendarBloc(),
          child: ListView(
            children: [
              Column(
                children: [
                  BlocBuilder<CalendarBloc, CalendarState>(
                    builder: (context, state) {
                      return Container(
                        height: 50,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 4, vertical: 4),
                        width: MediaQuery.of(context).size.width - 64,
                        decoration: BoxDecoration(
                          color: const Color(0XFF4541D1),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            filled: true,
                            fillColor: Color(0xFF0A0812),
                            hintText: "Nouvelle tâche...",
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                            border: InputBorder.none,
                            labelStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                          style: const TextStyle(
                            color: Colors.white70,
                          ),
                          onChanged: (value) {
                            context
                                .read<CalendarBloc>()
                                .add(OnTaskLabelChangedEvent(taskLabel: value));
                          },
                        ),
                      );
                    },
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 32.0,
                      vertical: 8.0,
                    ),
                    child: TaskCalendar(),
                  ),
                  BlocBuilder<CalendarBloc, CalendarState>(
                    builder: (context, state) {
                      String? time = "Start time";
                      if (state.timeOfDay != null) {
                        time = state.timeOfDay?.format(context);
                      }
                      String? endTime = "End time";
                      if (state.endOfDay != null) {
                        endTime = state.endOfDay?.format(context);
                      }

                      return Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 32.0,
                          vertical: 32.0,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  show().then((value) => {
                                        if (value != null)
                                          {
                                            context.read<CalendarBloc>().add(
                                                OnStartTimeSelectedEvent(
                                                    timeOfDay: value))
                                          }
                                      });
                                },
                                child: Container(
                                  height: 50,
                                  decoration: const BoxDecoration(
                                    //color: Color(0xFF020202),
                                    //color: Color(0xFF313842),
                                    color: Color(0XFF4541D1),
                                    border: Border.fromBorderSide(BorderSide()),
                                    shape: BoxShape.rectangle,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                  child: Row(
                                    children: [
                                      const Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Icon(
                                          Icons.access_time,
                                          size: 18.0,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        time.toString(),
                                        style: const TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  show().then((value) => {
                                        if (value != null)
                                          {
                                            context.read<CalendarBloc>().add(
                                                OnEndTimeSelectedEvent(
                                                    endTimeOfDay: value))
                                          }
                                      });
                                },
                                child: Container(
                                  height: 50,
                                  decoration: const BoxDecoration(
                                    //color: Color(0xFF020202),
                                    //color: Color(0xFF313842),
                                    color: Color(0XFF4541D1),
                                    border: Border.fromBorderSide(BorderSide()),
                                    shape: BoxShape.rectangle,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                  child: Row(
                                    children: [
                                      const Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Icon(
                                          Icons.access_time,
                                          size: 18.0,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        endTime.toString(),
                                        style: const TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  ElevatedButton(
                    child: Text('Show Notification'),
                    onPressed: () async {
                      await _notificationService.showNotifications();
                    },
                  ),
                  const TaskButtonSubmit(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
