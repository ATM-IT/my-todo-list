import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:mytodolist/state_management/calendar/calendar_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

class TaskCalendar extends StatelessWidget {
  const TaskCalendar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final kToday = DateTime.now();
    final kFirstDay = DateTime(kToday.year, kToday.month - 3, kToday.day);
    final kLastDay = DateTime(kToday.year, kToday.month + 3, kToday.day);
    DateTime _focusedDay = DateTime.now();
    DateTime? _selectedDay;

    return BlocBuilder<CalendarBloc, CalendarState>(
      builder: (context, state) {
        return TableCalendar(
          firstDay: kFirstDay,
          lastDay: kLastDay,
          focusedDay: _focusedDay,
          calendarFormat: CalendarFormat.month,
          startingDayOfWeek: StartingDayOfWeek.monday,
          availableGestures: AvailableGestures.none,
          headerVisible: true,
          availableCalendarFormats: const {
            CalendarFormat.month: 'Month',
          },
          headerStyle: const HeaderStyle(
            titleTextStyle: TextStyle(
              fontSize: 17.0,
              color: Colors.white,
              fontWeight: FontWeight.normal,
            ),
            formatButtonTextStyle: TextStyle(
              color: Colors.amber,
            ),
            formatButtonDecoration: BoxDecoration(
              border: Border.fromBorderSide(
                BorderSide(
                  color: Colors.amber,
                ),
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(1.0),
              ),
              color: Colors.red,
            ),
            leftChevronIcon: Icon(
              Icons.chevron_left,
              color: Colors.white,
            ),
            rightChevronIcon: Icon(
              Icons.chevron_right,
              color: Colors.white,
            ),
          ),
          calendarStyle: CalendarStyle(
            todayTextStyle: const TextStyle(
              color: Color(0XFF141417),
              fontWeight: FontWeight.bold,
            ),
            todayDecoration: BoxDecoration(
                color: const Color(0XFFB3C1FC),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(10.0)),
            weekendTextStyle: const TextStyle(color: Color(0XFFFFFFFF)),
            weekendDecoration: BoxDecoration(
                color: const Color(0XFF23262F),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(10.0)),
            outsideTextStyle: TextStyle(color: Colors.amberAccent),
            defaultTextStyle: const TextStyle(color: Color(0xFFFFFFFF)),
          ),
          daysOfWeekStyle: DaysOfWeekStyle(
            dowTextFormatter: (date, locale) {
              return DateFormat.E(locale).format(date).substring(0, 2);
            },
            weekdayStyle: const TextStyle(color: Colors.white),
            weekendStyle: const TextStyle(color: Colors.amber),
          ),
          selectedDayPredicate: (day) {
            return isSameDay(_selectedDay, day);
          },
          onDaySelected: (selectedDay, focusedDay) {
            context
                .read<CalendarBloc>()
                .add(OnDaySelectedEvent(selectedDate: selectedDay));

            print(selectedDay);
            if (!isSameDay(_selectedDay, selectedDay)) {
              print("issameday");
              _selectedDay = selectedDay;
              _focusedDay = focusedDay;
            }
          },
          onPageChanged: (focusedDay) {
            print(focusedDay);
            _focusedDay = focusedDay;
          },
          calendarBuilders: CalendarBuilders(
            /*markerBuilder: (context, date, holidays) {
              return Container(
                decoration: const BoxDecoration(
                  color: Color(0xFF30A9B2),
                  shape: BoxShape.circle,
                ),
                margin: const EdgeInsets.all(4.0),
                width: 4,
                height: 4,
              );
            },*/
            selectedBuilder: (context, date, _) {
              return Container(
                decoration: BoxDecoration(
                    color: const Color(0XFF7B8AF7),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(10.0)),
                margin: const EdgeInsets.all(4.0),
                width: 100,
                height: 100,
                child: Center(
                  child: Text(
                    '${date.day}',
                    style: const TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              );
            },
            /*
            headerTitleBuilder: (context, date) {
              final text = DateFormat.LLLL().format(date);
              return Container(
                decoration: const BoxDecoration(
                  color: Colors.transparent,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.all(0.0),
                height: 40,
                child: Center(
                  child: Text(
                    text,
                    style: const TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              );
            },

             */
            dowBuilder: (context, date) {
              final text = DateFormat.E().format(date).substring(0, 3);
              return Container(
                decoration: const BoxDecoration(
                  color: Colors.transparent,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.all(0.0),
                width: 100,
                height: 100,
                child: Center(
                  child: Text(
                    text,
                    style: const TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                        fontWeight: FontWeight.normal),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
