import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytodolist/state_management/calendar/calendar_bloc.dart';

class TaskButtonSubmit extends StatelessWidget {
  const TaskButtonSubmit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalendarBloc, CalendarState>(builder: (context, state) {
      return ElevatedButton(
        child: Text('Ajouter Tache'),
        onPressed: () async {
          context.read<CalendarBloc>().add(const OnTaskSubmittedEvent());
        },
      );
    });
  }
}
