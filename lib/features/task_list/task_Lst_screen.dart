import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mytodolist/core/box/task_box.dart';
import 'package:mytodolist/widget/base_screen.dart';

class TaskListScreen extends StatelessWidget {
  const TaskListScreen({Key? key}) : super(key: key);

  _getBody(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: Container(
            decoration: const BoxDecoration(
              color: Color(0xFF030303),
            ),
            child: ValueListenableBuilder(
              valueListenable: Hive.box("taskBox").listenable(),
              builder: (context, Box items, _) {
                List<String> keys = items.keys.cast<String>().toList();
                return ListView.builder(
                  itemCount: keys.length,
                  itemBuilder: (context, index) {
                    final task = items.get(keys[index]);
                    return Dismissible(
                      key: Key(task.label),
                      onDismissed: (direction) {
                        TaskBox.box.delete(task.key());
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("${task.label} supprimé")));
                      },
                      background: Container(
                        color: Colors.red,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        margin: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                      ),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        margin: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: const Color(0xFF2D2D37),
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0xFF030303), spreadRadius: 3),
                          ],
                        ),
                        child: Row(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  task.label,
                                  style: const TextStyle(
                                    color: Color(0xFFEBEEEF),
                                    fontSize: 14,
                                  ),
                                ),
                                Text(
                                  task.date,
                                  style: TextStyle(
                                    color: const Color(0xFFEBEEEF)
                                        .withOpacity(0.5),
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(_getBody(context));
  }
}
