import 'package:flutter/material.dart';
import 'package:mytodolist/features/dashboard/dashboard_screen.dart';
import 'package:mytodolist/features/new_task/new_task_screen.dart';
import 'package:mytodolist/features/task_list/task_Lst_screen.dart';

class TodoList extends StatefulWidget {
  const TodoList({Key? key}) : super(key: key);

  @override
  State<TodoList> createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  @override
  Widget build(BuildContext context) {
    void _redirectToPage(BuildContext context, Widget page) {
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        MaterialPageRoute newRoute =
            MaterialPageRoute(builder: (BuildContext context) => page);

        Navigator.of(context)
            .pushAndRemoveUntil(newRoute, ModalRoute.withName('/decision'));
      });
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (BuildContext context) => const DashboardScreen(),
        '/newTask': (BuildContext context) => const NewTaskScreen(),
        '/taskList': (BuildContext context) => const TaskListScreen(),
      },
    );
  }
}

/*
class _TodoListState extends State<TodoList> {
  @override
  Widget build(BuildContext context) {
    final _counterBloc = CounterBloc();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        backgroundColor: const Color(0xFFFCFCFD),
        appBar: AppBar(
          title: Text("AppBar"),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                child: Text('Entete du Drawer'),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                title: const Text('Item 1'),
                onTap: () {
                  _counterBloc.add(CounterIncrementPressed());
                },
              ),
              ListTile(
                title: const Text('Item 2'),
                onTap: () {
                  _counterBloc.add(CounterDecrementPressed());
                },
              ),
            ],
          ),
        ),
        body: BlocBuilder(
          bloc: _counterBloc,
          builder: (context, state) {
            return Row(
              children: [
                Center(
                    child: Text('You have pressed the button times. $state')),
                FloatingActionButton(
                  onPressed: () {
                    context.read<CounterBloc>().add(CounterIncrementPressed());
                  },
                ),
              ],
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _counterBloc.add(CounterIncrementPressed());
          },
          tooltip: 'Increment Counter',
          child: const Icon(Icons.add),
        ),
        bottomNavigationBar: const BottomNavBar(),
      ),
      routes: {
        '/dashboard': (_) => const DashboardScreen(),
        '/addTask': (BuildContext context) => const NewTaskScreen(),
      },
    );
  }
}
*/
