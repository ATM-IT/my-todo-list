part of 'counter_bloc.dart';

abstract class CounterState {
  final int count;

  const CounterState(this.count);
}

class CounterResult extends CounterState {
  const CounterResult(int count) : super(count);

  @override
  String toString() => 'CounterResult { count: $count }';
}
