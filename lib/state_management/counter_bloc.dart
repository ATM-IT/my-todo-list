import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(const CounterResult(0)) {
    on<CounterIncrementPressed>((event, emit) {
      _addToValue(1, emit);
    });
    on<CounterDecrementPressed>((event, emit) {
      _addToValue(-1, emit);
    });
  }


  void _addToValue(int toAdd, Emitter<CounterState> emit) {
    print('addToValue is $toAdd');

    if (state is CounterResult) {
      emit(CounterResult((state as CounterResult).count + toAdd));
    }
  }
}
