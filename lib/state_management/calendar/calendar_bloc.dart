import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:mytodolist/core/box/task_box.dart';
import 'package:mytodolist/core/model/task_model.dart';

part 'calendar_event.dart';

part 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {
  CalendarBloc() : super(CalendarState(DateTime.now(), null, null, "")) {
    on<OnDaySelectedEvent>((event, emit) {
      print("coucou");
      print(event.selectedDate);
      //emit(CalendarState(event.selectedDate, null));
      emit(state.copyWith(selectedDay: () => event.selectedDate));
    });

    on<OnStartTimeSelectedEvent>((event, emit) {
      print("timeOfDay");
      print(event.timeOfDay);
      emit(state.copyWith(timeOfDay: () => event.timeOfDay));
    });

    on<OnEndTimeSelectedEvent>((event, emit) {
      print("endtimeOfDay");
      print(event.endTimeOfDay);
      emit(state.copyWith(endOfDay: () => event.endTimeOfDay));
    });

    on<OnTaskLabelChangedEvent>((event, emit) {
      print("taskLabel");
      print(event.taskLabel);
      emit(state.copyWith(taskLabel: () => event.taskLabel));
    });

    on<OnTaskSubmittedEvent>((event, emit) {
      print("taskSubmited");
      print(state.taskLabel);
      TaskModel taskModel = TaskModel("4", state.taskLabel, "category",
          "priority", DateFormat.yMMMMd(Platform.localeName).format(state.selectedDay));
      TaskBox.box.put(taskModel.key(), taskModel);
    });
  }
}
