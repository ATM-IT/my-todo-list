part of 'calendar_bloc.dart';

@immutable
class CalendarState {
  final DateTime selectedDay;
  final TimeOfDay? timeOfDay;
  final TimeOfDay? endOfDay;
  final String taskLabel;

  const CalendarState(
      this.selectedDay, this.timeOfDay, this.endOfDay, this.taskLabel);

  CalendarState copyWith({
    DateTime Function()? selectedDay,
    TimeOfDay Function()? timeOfDay,
    TimeOfDay Function()? endOfDay,
    String Function()? taskLabel,
  }) {
    return CalendarState(
        selectedDay != null ? selectedDay() : this.selectedDay,
        timeOfDay != null ? timeOfDay() : this.timeOfDay,
        endOfDay != null ? endOfDay() : this.endOfDay,
        taskLabel != null ? taskLabel() : this.taskLabel);
  }
}
