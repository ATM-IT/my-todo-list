part of 'calendar_bloc.dart';

@immutable
abstract class CalendarEvent {
  const CalendarEvent();
}

class OnDaySelectedEvent extends CalendarEvent {
  final DateTime selectedDate;

  const OnDaySelectedEvent({required this.selectedDate});
}

class OnPageChangedEvent extends CalendarEvent {}

class OnFormatChangedEvent extends CalendarEvent {}

class OnSelectedDayPredicateEvent extends CalendarEvent {}

class OnStartTimeSelectedEvent extends CalendarEvent {
  final TimeOfDay timeOfDay;

  const OnStartTimeSelectedEvent({required this.timeOfDay});
}

class OnEndTimeSelectedEvent extends CalendarEvent {
  final TimeOfDay endTimeOfDay;

  const OnEndTimeSelectedEvent({required this.endTimeOfDay});
}

class OnTaskLabelChangedEvent extends CalendarEvent {
  final String taskLabel;

  const OnTaskLabelChangedEvent({required this.taskLabel});
}

class OnTaskSubmittedEvent extends CalendarEvent {


  const OnTaskSubmittedEvent();
}
