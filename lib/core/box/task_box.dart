import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mytodolist/core/model/task_model.dart';
import 'package:path_provider/path_provider.dart';

class TaskBox {
  static final TaskBox instance = TaskBox();
  static late Box box;

  static void init() async {
    final dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    Hive.registerAdapter(TaskModelAdapter());
    box = await Hive.openBox("taskBox");
    var values = box.values;
    if (values == null || values.isEmpty) {
      TaskBox.box.putAll({for (var e in tasks) e.key(): e});
    }
  }

  static final List<TaskModel> tasks = [
    TaskModel("1", "Courir 10 KM", "Sport", "Faible", "10/02/2022"),
    TaskModel("2", "Courir 30 KM", "Sport", "Faible", "10/02/2022"),
    TaskModel("3", "Courir 20 KM", "Sport", "Faible", "10/02/2022"),
  ];
}
