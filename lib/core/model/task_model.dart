import 'package:hive/hive.dart';

part 'task_model.g.dart';

@HiveType(typeId: 0)
class TaskModel {
  @HiveField(0)
  String id;

  @HiveField(1)
  String label;

  @HiveField(2)
  String category;

  @HiveField(3)
  String priority;

  @HiveField(4)
  String date;

  @HiveField(5)
  String? startDate;

  @HiveField(6)
  String? endDate;

  @HiveField(7)
  bool? scheduler;

  TaskModel(this.id, this.label, this.category, this.priority, this.date);

  String key() => label.hashCode.toString();
}
