import 'package:hive/hive.dart';

part 'task_category_model.g.dart';

@HiveType(typeId: 1)
class TaskCategoryModel {
  @HiveField(0)
  String label;

  @HiveField(1)
  HiveList? taskList;

  TaskCategoryModel(
    this.label,
  );
}
