import 'package:flutter/material.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  Widget _addPadding(Widget child) => Container(
        //padding: const EdgeInsets.symmetric(horizontal: 8),
        height: 55,
        decoration: BoxDecoration(
          //color: const Color(0XFF4541D1),
          //color: const Color(0XFF28EDA0),
          //color: const Color(0XFF4541D1),
          color: const Color(0XFFA75FFF),
          //color: const Color(0XFF2A2A2A),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(1.0),
        ),
        child: Center(
          child: child,
        ),
      );

  Widget _getMenuItem(BuildContext context,
      {required Icon icon, required String routeName}) {
    return _addPadding(
      IconButton(
        icon: icon,
        iconSize: 24,
        onPressed: () => Navigator.pushNamed(context, routeName),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      notchMargin: 8,
      shape: const CircularNotchedRectangle(),
      color: const Color(0xFF030303),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: _getMenuItem(
              context,
              icon: const Icon(
                Icons.home,
                size: 24.0,
                color: Color(0xFFFFFFFF),
              ),
              routeName: "/",
            ),
          ),
          Expanded(
              flex: 1,
              child: _getMenuItem(
                context,
                icon: const Icon(
                  Icons.apps_sharp,
                  size: 24.0,
                  color: Color(0xFFFFFFFF),
                ),
                routeName: "/taskList",
              )),
        ],
      ),
    );
  }
}
