import 'package:flutter/material.dart';
import 'package:mytodolist/widget/bottom_nav_bar.dart';

class BaseScreen extends StatelessWidget {
  final Widget myBody;

  const BaseScreen(this.myBody, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "/newTask");
        },
        tooltip: 'Increment',
        child: const Icon(
          Icons.add,
          size: 24.0,
          color: Color(0xFF030303),
        ),
        elevation: 2.0,
        //backgroundColor: const Color(0xFF030303),
        backgroundColor: const Color(0xFF28EDA0),
        //backgroundColor: Color(0xFF5178FF),
      ),
      bottomNavigationBar: const BottomNavBar(),
      body: myBody,
    );
  }
}
