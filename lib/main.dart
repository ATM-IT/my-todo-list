import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mytodolist/core/box/task_box.dart';
import 'package:mytodolist/core/model/task_category_model.dart';
import 'package:mytodolist/core/service/notification_service.dart';
import 'package:mytodolist/todolist.dart';

const String tasksBoxName = "tasks";


void main() async {
  //runApp(const TodoList());
  WidgetsFlutterBinding.ensureInitialized();
  NotificationService().init();
  /*await Hive.initFlutter();
  Hive.registerAdapter<TaskCategoryModel>(TaskCategoryModelAdapter());
  await Hive.openBox<TaskCategoryModel>(tasksBoxName);


   */
  TaskBox.init();
  initializeDateFormatting("fr").then((_) => runApp(const TodoList()));
}
